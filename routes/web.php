<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



/*
 ---------------------------------------------------------
  ROUTES :
 ---------------------------------------------------------
*/

/*
  ROUTE PAR DEFAUT
  PATERN: /
  CTRL: Pages
  ACTION: index
*/
Route::get('/', 'PagesController@show')->defaults('page', 1)->name('app');

/*
  ROUTE DETAILS D'UNE PAGE
  PATERN: /pages/id/slug.html
  CTRL: Pages
  ACTION: show
*/
Route::get('/pages/{page}/{slug}.html', 'PagesController@show')
      ->where([
          'pages' => '[1-9][0-9]*',
          'slug' => '[a-z0-9][a-z0-9\-]*[a-z0-9]'
      ])
      ->name('pages.show');


/*
  ROUTES DES CREATURES
*/
Route::prefix('creatures')->name('creatures.')->group(function (){
  /*
    ROUTE DETAILS D'UNE CREATURE
    PATERN: /creatures/id/slug.html
    CTRL: CreaturesController
    ACTION: show
  */
  Route::get('/{creature}/{slug}.html', 'CreaturesController@show')
        ->where([
              'creature' => '[1-9][0-9]*',
              'slug' => '[a-z0-9][a-z0-9\-]*[a-z0-9]'
            ])
        ->name('show');
});


/*
  ROUTE DES FILMS
*/
Route::prefix('films')->name('films.')->group(function (){
  /*
    ROUTE DETAILS D'UN FILM
    PATERN: /films/id/slug.html
    CTRL: FilmsController
    Action: show
  */
  Route::get('/{film}/{slug}.html', 'FilmsController@show')
  ->where([
        'film' => '[1-9][0-9]*',
        'slug' => '[a-z0-9][a-z0-9\-]*[a-z0-9]'
      ])
  ->name('show');

  /*
    FORMULAIRE D'AJOUT D'UN FILM
    PATTERN: /films/create/slug.html
    CTRL: Films
    ACTION: create
  */
    Route::get('/create/{slug}.html','FilmsController@create')
        ->where([
                'slug' => '[a-z0-9][a-z0-9\-]*[a-z0-9]'
              ])
          ->name('create');

  /*
    INSERT D'UN FILM
    PATTERN: /films [POST]
    CTRL: Films
    ACTION: store
  */
    Route::post('/','FilmsController@store')
          ->name('store');

  /*
    FORMULAIRE DE L'EDIT D'UN FILM
    PATTERN: /films/id/edit
    CTRL: Films
    ACTION: edit
   */
    Route::get('/{film}/edit/{slug}.html','FilmsController@edit')
           ->where([
              'film' => '[1-9][0-9]*',
              'slug' => '[a-z0-9][a-z0-9\-]*[a-z0-9]'
             ])
          ->name('edit');

  /*
    UPDATE D'UN FILM
    PATTERN: /films/{films} [PATCH]
    CTRL: Films
    ACTION: update
   */
    Route::patch('/{film}','FilmsController@update')
           ->where([
              'film' => '[1-9][0-9]*'
             ])
          ->name('update');

  /*
    ROUTE DE LA SUPPRESSION D'UN FILM
    PATTERN: /films/{film} [DELETE]
    CTRL: Films
    ACTION: destroy
   */
    Route::delete('/{film}','FilmsController@destroy')
           ->where([
              'film' => '[1-9][0-9]*'
             ])
          ->name('destroy');
});

/*
  ROUTE DETAILS D'UN TAG
  PATTERN: /tags/id/slug.html
  CTRL: Tags
  ACTION: show
*/
Route::get('/tags/{tag}/{slug}.html', 'TagsController@show')
      ->where([
            'tag' => '[1-9][0-9]*',
            'slug' => '[a-z0-9][a-z0-9\-]*'
      ])
      ->name('tags.show');

/*
  ROUTE DE LA RECHERCHE
  PATTERN: /search
  CTRL: Creatures
  ACTION: search
*/
Route::get('/search', 'CreaturesController@search')
      ->name('creatures.search');
