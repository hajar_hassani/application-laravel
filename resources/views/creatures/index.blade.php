{{--
  ./resources/views/creatures/index.blade.php
  Liste des créatures
  vairables disponibles :
      - $creatures
 --}}

@foreach ($creatures as $creature)
  <!-- Project One -->
  <div class="row">
    <div class="col-md-4">
      <a href="#">
        <img class="img-fluid rounded mb-3 mb-md-0" src="{{ asset('images/'.$creature->image) }}" alt="">
      </a>
    </div>
    <div class="col-md-8">
      <h3>{{ $creature->nom }}</h3>
      <p class="lead">
        dans
        <a href="artiste_details.html">Ze craignos monster</a>
        le {{ \Carbon\Carbon::parse ($creature->created_at)->format('d-m-Y')}}
      </p>
      <p>{{ $creature->texteLead}}</p>
      <a class="btn btn-primary" href="{{ URL::route('creatures.show', [
          'creature' => $creature->id,
          'slug'     => Str::slug($creature->nom)
        ]) }}">
        Voir la créature
      </a>
      <hr/>
      <ul class="list-inline tags">
        @foreach ($creature->tags as $tag)
          <li>
            <a href="{{ URL::route('tags.show', [
              'tag'  => $tag->id,
              'slug' => Str::slug($tag->nom, '-')
              ]) }}" class="btn btn-default btn-xs">
              {{ $tag->nom }}
            </a>
          </li>
        @endforeach
      </ul>
    </div>
  </div>
  <!-- /.row -->
  <hr>
@endforeach
