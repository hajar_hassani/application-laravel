{{--
  ./resources/views/pages/show.blade.php
  variables disponibles :
      - $pages Page
      - Si $page-id =1 : $creatures
 --}}

 @extends('template.app')

 @section('titre')
   Les Créatures du Futur
 @endsection

 @section('content1')
   <!-- Page Heading -->
   <!-- Title -->
   <h1 class="mt-4">{{ $page->titre }}</h1>

   <hr>

   <!-- Post Content -->
   <p class="lead">{{ $page->texte }}</p>

   <hr>

<!-- Intégrations des vues complémentaires -->
   @if ($page->id === 1)
     <!-- Title -->
     <h2 class="mt-4">Dernieres créatures</h2>
     <hr>
     @include('creatures.index')
   @elseif ($page->id === 2)
     <a class="btn btn-primary" href="{{ URL::route('films.create', [
       'slug' => Str::slug($page->titre)
       ]) }}">Ajouter un film</a>
     <hr/>
     @include('films.index')
     <hr>
   @elseif ($page->id === 3)
     @include('creatures.index')
     <hr>
   @endif

 @endsection
