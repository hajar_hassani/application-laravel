{{--
  ./resources/views/pages/_menu.blade.php
  variables disponibles :
      - $pages Pages
 --}}


<ul class="navbar-nav ml-auto">
  @foreach ($pages as $page)
    <li class="nav-item active">
      <a class="nav-link" href="{{ URL::route('pages.show', [
          'page' => $page->id,
          'slug' => Str::slug($page->titre)
          ]) }}">
        {{ $page->titre }}
      </a>
    </li>
  @endforeach
</ul>
