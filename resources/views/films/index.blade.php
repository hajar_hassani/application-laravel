{{--
  ./resources/views/films/index.blade.php
  Liste des films
  vairables disponibles :
      - $films
 --}}

@foreach ($films as $film)
<!-- Project One -->
<div class="row">
  <div class="col-md-4">
    <a href="#">
      <img class="img-fluid rounded mb-3 mb-md-0" src="{{ asset('images/'.$film->image) }}" alt="">
    </a>
  </div>
  <div class="col-md-8">
    <h3>{{ $film->titre }}</h3>
    <p>{{ $film->synopsis}}</p>
    <a class="btn btn-primary" href="{{ URL::route('films.show', [
      'film'  =>  $film->id,
      'slug'  => Str::slug($film->titre)
      ]) }}">
      Voir les détails du film
    </a>
    <hr/>
  </div>
</div>
<hr/>
@endforeach
