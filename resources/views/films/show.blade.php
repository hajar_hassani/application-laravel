{{--
  ./resources/views/films/show.blade.php
  variables disponibles :
      - $film
 --}}
 @extends('template.app')

 @section('titre')
   Les Films
 @endsection

 @section('content1')
    <!-- Page Heading -->

    <!-- Title -->
    <h1 class="mt-4">{{ $film->titre }}</h1>

    <hr>

    <!-- Project One -->
    <div class="row">
      <div class="col-md-4">
        <a href="#">
          <img class="img-fluid rounded mb-3 mb-md-0" src="{{ asset('images/'.$film->image) }}" alt="">
        </a>
      </div>
      <div class="col-md-8">
        <p class="lead">{{ $film->synopsis }}</p>
        <hr/>
        <h2>Créatures du film</h2>
        <ul>

          @foreach ($film->creatures as $creature)
            <li><a href="{{  URL::route('creatures.show', [
                'creature' => $creature->id,
                'slug'     => Str::slug($creature->nom)
              ]) }}">
              {{ $creature->nom }}
            </a>
          </li>
          @endforeach
        </ul>
        <hr/>
          <a class="btn btn-primary" href="{{ URL::route('films.edit',  [
              'film' => $film->id,
              'slug' => Str::slug($film->titre)
              ]) }}">Modifier</a>
          <form method="POST" action="{{ route('films.destroy', $film->id) }}" class="btn">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
            <div class="form-group">
              <input type="submit" class="btn btn-danger" value="Delete film">
            </div>
          </form>

        <hr />

      </div>
    </div>
    <!-- /.row -->
    <hr>
@endsection
