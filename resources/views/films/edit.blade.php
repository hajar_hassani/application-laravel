{{--
  ./resources/views/films/edit.blade.php
  Variable disponible:
    - $film Film
 --}}

 @extends('template.app')

 @section('titre')
   Les Créatures du Futur
 @endsection

 @section('content1')
<h1 class="mt-4">Modifier un Film</h1>

<form method="POST" action="{{ route('films.update', $film->id) }}">
  @csrf
    {{ method_field('PATCH') }}
  <div class="form-group">
    <label for="">Titre</label>
    <input type="text" class="form-control" value="{{ $film->nom }}" name="titre"/>
  </div>
  <div class="form-group">
    <label for="">Synopsis</label>
    <textarea rows="8" cols="80" class="form-control" name="synopsis"></textarea>
  </div>
  <div class="form-group">
    <input type="submit" class="btn btn-primary" />
  </div>
</form>

<!-- /.row -->
<hr>
@endsection
