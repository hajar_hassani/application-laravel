{{--
  ./resources/views/template/app.blade.php
  Template général
 --}}

<!DOCTYPE html>
<html lang="en">
  <head>
    @include('template.partials._head')
  </head>
<body>

    <!-- Navigation -->
    @include('template.partials._nav')

    <!-- Page Content -->
    <div class="container">
      <div class="row">
        <!-- Post Content Column -->
        <div class="col-lg-8">

          @yield('content1')

        </div>

        <!-- Sidebar Widgets Column -->
        <div class="col-md-4">
          @include('template.partials._sidebar')
        </div>

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-dark">
      @include('template.partials._footer')
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    @include('template.partials._scripts')
  </body>

</html>
