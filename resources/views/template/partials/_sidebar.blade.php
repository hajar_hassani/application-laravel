
<!-- Search Widget -->
<form action="{{ route('creatures.search') }}" method="get">
  <div class="card my-4">
    <h5 class="card-header">Rechercher une créature</h5>
    <div class="card-body">
      <div class="input-group">
        <input type="text" name="query" value="{{ request()->input('query') }}" class="form-control" placeholder="Mot-clé">
        <span class="input-group-btn">
          <button class="btn btn-secondary" type="submit">Go!</button>
        </span>
      </div>
    </div>
  </div>
</form>


<!-- Categories Widget -->
<div class="card my-4">
  @include('tags._menu')
</div>
