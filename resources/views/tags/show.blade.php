{{--
  ./resources/views/tags/show.blade.php
  variables disponibles :
      - $tag Tag
 --}}

@extends('template.app')

@section('titre')
  Posts du tag {{ $tag->nom }}
@endsection

@section ('content1')
  <!-- Page Heading -->
  <!-- Title -->
  <h1 class="mt-4">{{ $tag->nom }}</h1>
  <p class="lead">Liste des créatures</p>

  <hr>

  @foreach ($tag->creatures as $creature)
    <!-- Project One -->
    <div class="row">
      <div class="col-md-4">
        <a href="#">
          <img class="img-fluid rounded mb-3 mb-md-0" src="{{ asset('images/'.$creature->image) }}" alt="">
        </a>
      </div>
      <div class="col-md-8">
        <h3>{{ $creature->nom }}</h3>
        <p class="lead">
          dans
          <a href="artiste_details.html">Ze craignos monster</a>
          le {{ \Carbon\Carbon::parse ($creature->created_at)->format('d-m-Y')}}
        </p>
        <p>{{ $creature->texteLead}}</p>
        <a class="btn btn-primary" href="{{ URL::route('creatures.show', [
            'creature' => $creature->id,
            'slug'     => Str::slug($creature->nom)
          ]) }}">
          Voir la créature
        </a>
        
        <hr/>

      </div>
    </div>
    <!-- /.row -->

    <hr>
  @endforeach

@endsection
