{{--
  ./resources/views/tags/menu.blade.php
  variables disponibles :
      - $tags Array(Tag)
 --}}

<h5 class="card-header">Tags</h5>
<div class="card-body">
  <div class="row">
    <div class="col-lg-12">
      <ul class="list-unstyled mb-0">
        @foreach ($tags as $tag)
          <li>
            <a href="{{ URL::route('tags.show', [
              'tag'  => $tag->id,
              'slug' => Str::slug($tag->nom, '-')
              ]) }}">
              {{ $tag->nom }}
            </a>
          </li>
        @endforeach
      </ul>
    </div>
  </div>
</div>
