<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider {
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
          View::composer('tags._menu', function($view){
            $view->with('tags', \App\Http\Models\Tag::all());
          });

          View::composer('films.show', function($view){
            $view->with('creatures', \App\Http\Models\Creature::all());
          });

          View::composer('pages.menu', function($view){
            $view->with('pages', App\Http\Models\Page::all());
          });

    }

}
