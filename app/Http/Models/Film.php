<?php
   namespace App\Http\Models;
   use Illuminate\Database\Eloquent\Model;

   class Film extends Model {
     /**
      * The table associated with the Model
      * @var string
      */
      protected $fillable = ['titre', 'synopsis'];

      /**
       * Get the posts for the categorie.
       */
      public function creatures() {
          return $this->hasMany('App\Http\Models\Creature', 'film');
      }
   }
