<?php
   namespace App\Http\Models;
   use Illuminate\Database\Eloquent\Model;

   class Page extends Model {

     /**
      * The table associated with the Model
      * @var string
      */
     protected $table = 'pages';

   }
