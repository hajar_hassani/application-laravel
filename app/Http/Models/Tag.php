<?php
namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model {

  protected $fillable = ['nom'];
  /**
   * Get the posts of the tag.
   */
  public function creatures() {
    return $this->belongsToMany('App\Http\Models\Creature',
                                  'creatures_has_tags', 'creature', 'tag');
  }

}
