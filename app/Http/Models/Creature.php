<?php
   namespace App\Http\Models;
   use Illuminate\Database\Eloquent\Model;

   class Creature extends Model {
     /**
      * The table associated with the Model
      * @var string
      */
      protected $table = 'creatures';

      /**
       * Get the categorie that owns the post.
       */
      public function film(){
          return $this->belongsTo('App\Http\Models\Film', 'film');
      }

      /**
       * Get the tags of the post.
       */
      public function tags() {
        return $this->belongsToMany('App\Http\Models\Tag',
                                    'creatures_has_tags', 'creature', 'tag');
      }
   }
