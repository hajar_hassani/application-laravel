<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\View;
use App\Http\Models\Page;
use App\Http\Models\Creature;
use App\Http\Models\Film;
use Illuminate\Support\Facades\DB;

class PagesController extends Controller {

  /**
   * Détails de la pages $id
   * @param integer $id [id de la page à afficher]
   *@return View       [Vue pages/show.blade.php]
  */

  public function show($id = 1){
    $page = Page::find($id);
    if ($page->id === 1):
      $creatures = Creature::orderBy('created_at', 'desc')->take(2)->get();
      return View::make('pages.show', compact('page', 'creatures'));
    elseif ($page->id === 2):
      $films = Film::orderBy('created_at', 'desc')->get();
      return View::make('pages.show', compact('page', 'films'));
    elseif ($page->id === 3):
      $creatures = Creature::orderBy('created_at', 'desc')->get();
      return View::make('pages.show', compact('page', 'creatures'));
    endif;
    return View::make('pages.show', compact('page'));

    /*$pages = Page::all();
    return View::make('pages.show',['pages' => $pages]);
    */
  }
}
