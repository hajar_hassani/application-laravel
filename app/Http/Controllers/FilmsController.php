<?php
   namespace App\Http\Controllers;

   use Illuminate\Support\Facades\View;
   use App\Http\Models\Film;
   use App\Http\Models\Page;
   use Illuminate\Http\Request;
   use Illuminate\Support\Facades\DB;


   class FilmsController extends Controller {

     public function show(Film $film) {
       return View::make('films.show',compact('film'));
     }

     public function create() {
       return View::make('films.create');
     }

     public function store(Request $request) {
       Film::create($request->all());
       return redirect()->route('pages.show', ['page' => 2, 'slug' => 'films']);
     }

     public function edit(Film $film) {
       return View::make('films.edit', compact('film'));
     }

     public function update(Request $request, Film $film) {
       $film->update($request->all());
       return redirect()->route('pages.show', ['page' => 2, 'slug' => 'films']);
     }

     public function destroy(Film $film) {
       $film->destroy($film->id);
       return redirect()->route('pages.show', ['page' => 2, 'slug' => 'films']);
     }

   }
