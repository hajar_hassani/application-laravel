<?php
   namespace App\Http\Controllers;

   use Illuminate\Support\Facades\View;
   use App\Http\Models\Creature;
   use Illuminate\Http\Request;
   use Illuminate\Support\Facades\DB;

   class CreaturesController extends Controller {

     /**
      * Affiche le detail d'une créature
      *@param Creature $creature
      *@return vue creatures.show
      */

     public function show($id){
       $creature = Creature::find($id);
       return View::make('creatures.show',compact('creature'));
     }


     /**
     *Affiche le resultat de la recherche
     *@param Request $request
     *@return vue creatures.search-results
     */
     public function search(Request $request) {
       $request->validate([
         'query' => 'required|min:1'
       ]);
       $query = $request->input('query');
       $creatures = Creature::where('nom', 'LIKE', "%$query%")->get();
       return View::make('creatures.search')->with('creatures', $creatures);
     }
   }
